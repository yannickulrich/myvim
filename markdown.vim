fun! InitMarkDown()
    python3 <<EOF
import requests
import subprocess
import sys
import re
from email.utils import parsedate
import datetime
import os

def pretty_date(time=False):
    """
    Get a datetime object or a int() Epoch timestamp and return a
    pretty string like 'an hour ago', 'Yesterday', '3 months ago',
    'just now', etc
    """
    from datetime import datetime
    now = datetime.now()
    if type(time) is int:
        diff = now - datetime.fromtimestamp(time)
    elif isinstance(time,datetime):
        diff = now - time
    elif not time:
        diff = now - now
    second_diff = diff.seconds
    day_diff = diff.days

    if day_diff < 0:
        return ''

    if day_diff == 0:
        if second_diff < 10:
            return "just now"
        if second_diff < 60:
            return str(second_diff) + " seconds ago"
        if second_diff < 120:
            return "a minute ago"
        if second_diff < 3600:
            return str(second_diff / 60) + " minutes ago"
        if second_diff < 7200:
            return "an hour ago"
        if second_diff < 86400:
            return str(second_diff / 3600) + " hours ago"
    if day_diff == 1:
        return "Yesterday"
    if day_diff < 7:
        return str(day_diff) + " days ago"
    if day_diff < 31:
        return str(day_diff / 7) + " weeks ago"
    if day_diff < 365:
        return str(day_diff / 30) + " months ago"
    return str(day_diff / 365) + " years ago"


header = """
<!DOCTYPE html>
<html>
  <head>
    <script>
window.gon={};gon.user_color_scheme="white";

function myRender()
{
    var math = document.getElementsByClassName('js-render-math');
    for (var i = 0; i < math.length; i++) {
        katex.render(
            math[i].textContent,
            math[i],{
                displayMode:math[i].getAttribute('data-math-style') == 'display'
            }
        );
    }
}

window.onload = myRender;
    </script>

<link rel="stylesheet" href="https://assets.gitlab-static.net/assets/application-377911057c10792b87ab3985612082b614bb40b05eb9d701e741e3a547834ec2.css">
<link rel="stylesheet" href="https://assets.gitlab-static.net/assets/highlight/themes/white-a9ac9757d6459344060c9834d594504c87bd5cab6ec60038ceb507ca95d38121.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.12.0/dist/katex.min.css" integrity="sha384-AfEj0r4/OFrOo5t7NnNe46zW/tFgW6x/bCJG8FqQCEo3+Aro6EYUG4+cU+KJWu/X" crossorigin="anonymous">

    <!-- The loading of KaTeX is deferred to speed up page rendering -->
    <script defer src="https://cdn.jsdelivr.net/npm/katex@0.12.0/dist/katex.min.js" integrity="sha384-g7c+Jr9ZivxKLnZTDUhnkOnsh30B4H0rpLUpJ4jAIKs4fnJI+sEnkvrMWph2EDg4" crossorigin="anonymous"></script>

    <script src="https://assets.gitlab-static.net/assets/webpack/runtime.27ff8ac7.bundle.js"></script>
    <script src="https://assets.gitlab-static.net/assets/webpack/main.420e8be6.chunk.js"></script>
    <script src="https://assets.gitlab-static.net/assets/webpack/sentry.e326ff4e.chunk.js"></script>
    <script src="https://assets.gitlab-static.net/assets/webpack/graphql.99c2509f.chunk.js"></script>
    <script src="https://assets.gitlab-static.net/assets/webpack/commons-pages.admin-pages.admin.abuse_reports-pages.admin.application_settings-pages.admin.applicati-83a868d1.9e9d071f.chunk.js"></script>
    <script src="https://assets.gitlab-static.net/assets/webpack/commons-globalSearch-pages.admin.abuse_reports-pages.admin.application_settings.ci_cd-pages.admin.gr-1b92290f.5c1a6601.chunk.js"></script>
    <script src="https://assets.gitlab-static.net/assets/webpack/commons-pages.groups.boards-pages.groups.details-pages.groups.epic_boards-pages.groups.show-pages.gr-c4b05faa.7a829a39.chunk.js"></script>
    <script src="https://assets.gitlab-static.net/assets/webpack/commons-pages.admin.audit_logs-pages.admin.impersonation_tokens-pages.analytics.productivity_analyti-70b98031.189cbab7.chunk.js"></script>
    <script src="https://assets.gitlab-static.net/assets/webpack/commons-pages.groups.epics.index-pages.groups.epics.new-pages.groups.epics.show-pages.groups.iterati-56adde80.842bafc7.chunk.js"></script>
    <script src="https://assets.gitlab-static.net/assets/webpack/commons-pages.groups.details-pages.groups.edit-pages.groups.group_members-pages.groups.show-pages.pr-772f311b.1be98c84.chunk.js"></script>
    <script src="https://assets.gitlab-static.net/assets/webpack/commons-pages.projects.blame.show-pages.projects.blob.edit-pages.projects.blob.new-pages.projects.bl-c6edf1dd.e3e77f68.chunk.js"></script>
    <script src="https://assets.gitlab-static.net/assets/webpack/commons-pages.groups.details-pages.groups.group_members-pages.groups.show-pages.projects.project_mem-4fea0e4f.25bb94a9.chunk.js"></script>
    <script src="https://assets.gitlab-static.net/assets/webpack/commons-pages.groups.details-pages.groups.show-pages.profiles.notifications.show-pages.projects.show.57a3456e.chunk.js"></script>
    <script src="https://assets.gitlab-static.net/assets/webpack/commons-pages.projects.show-pages.projects.tree.show.6dc9170b.chunk.js"></script>
    <script src="https://assets.gitlab-static.net/assets/webpack/pages.projects.show.175a92e6.chunk.js"></script>


    <style>
        .math{
            background-color:#fff;
            border:0;
        }
        #body {
            padding: 0 16px;
            max-width: 990px;
            width: 100%;
        }
    </style>
"""
body = """
    <title>GitLab rendered Markdown: %s</title>
  </head>
  <body>
    <div id="body">
      <div class="wiki-page-header">
        <div class="nav-text">
          <h2 class="wiki-page-title">%s</h2>
          <span class="wiki-last-edit-by">
            Last edited by <strong>%s</strong>
            %s
          </span>
        </div>
      </div>
      <div class="md wiki prepend-top-default">
"""
footer = """
      </div>
    </div>
  </body>
</html>
"""




def get_token():
    proc = subprocess.Popen(["pass","gitlab-token"],stdout=subprocess.PIPE)
    return proc.communicate()[0].splitlines()[0]


def refresh(filename):
    proc = subprocess.Popen(['xdotool','getactivewindow'], stdout=subprocess.PIPE)
    cwid = proc.communicate()[0].splitlines()[0]

    proc=subprocess.Popen(['xdotool','search','--name','GitLab rendered Markdown'], stdout=subprocess.PIPE)
    wids = proc.communicate()[0].splitlines()
    if len(wids) == 0:
        proc = subprocess.Popen(['firefox', filename], close_fds=True)
        return
    for wid in wids:
        proc = subprocess.Popen(['xdotool', 'windowactivate', wid])
        proc = subprocess.Popen(['xdotool', 'key', 'F5'])
    proc = subprocess.Popen(['xdotool', 'windowactivate', cwid])



def render(md, token, project):
    r = requests.post(
        "https://gitlab.com/api/v4/markdown",
        headers={"Private-Token": token},
        data={
            "text": md, "gfm": True, "project": project
        }
    )
    if r.status_code != 201:
        print("Failed!")
        print(r.text)
        raise SystemExit
    else:
        return r.json()['html']


def getmeta(filename):
    proc = subprocess.Popen([
        'git','log','-n','1', '--', os.path.basename(filename)
        ], cwd=os.path.realpath(os.path.dirname(filename)),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )
    stdout,stderr = proc.communicate()
    if not stdout:
        return 'Unknown', 'Untracked'

    stdout = stdout.decode('ascii')
    author = re.findall("Author: (.*) <.*>",stdout)[0]
    ds = re.findall('Date: *(.*)', stdout)[0]
    ds = parsedate(ds)
    diff = pretty_date(datetime.datetime(*ds[:6]))
    return author,diff


def getproject(filename):
    proc = subprocess.Popen([
        'git','remote','-v'
        ], cwd=os.path.realpath(os.path.dirname(filename)),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )
    stdout,stderr=proc.communicate()
    if not stdout:
        return 'mule-tools/monte-carlo'
    server, project = re.findall("origin\tgit@([^:]*):([^ ]*).git", stdout.decode())[0]
    return project


def md2html(filename):
    with open(filename) as fp:
        md = fp.read()
    html = render(md, get_token(), getproject(filename).replace('.wiki',''))
    luser, date = getmeta(filename)
    return header + (body % (
        filename.replace('-',' '), filename.replace('-',' '), luser, date
    )) + html + footer


def CompileMarkDown():
    html = md2html(vim.current.buffer.name)
    print("Done!")
    with open('/tmp/text.html','w') as fp:
        fp.write(html)
    refresh('/tmp/text.html')


EOF

    setlocal spell spelllang=en_gb
    setlocal tw=70
    nmap <buffer> <C-T> :python3 CompileMarkDown()<cr>
endfun
autocmd BufNewFile,BufRead *.md call InitMarkDown()
