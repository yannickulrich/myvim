let b:timetravel = 0
let b:fasttimetravel = 0


fun! StartTimeMachine()
    let l:changes = split(execute("undolist"), "\n")[-1]
    let l:n = split(changes, " ")[0]
    let b:nhistory = l:n
    let b:timezone = l:n
    let b:timetravel = 1

    nmap <buffer> M :call MoveTimezone(+1)<cr>
    nmap <buffer> m :call MoveTimezone(-1)<cr>
    nmap <buffer> s :call SaveTimezone()<cr>
    nmap <buffer> a :call SaveAllTimezones()<cr>
    nmap <buffer> q :call StopTimeMachine()<cr>

    nmap <buffer> i :call FastTimetravel()<cr>


    set nomodifiable
endfun


fun! StopTimeMachine()
    if b:timetravel
        set modifiable
        let b:timetravel = 0
        unmap <buffer> M
        unmap <buffer> m
        unmap <buffer> s
        unmap <buffer> q
        unmap <buffer> a
        unmap <buffer> i
        if b:fasttimetravel
            unmap <buffer> y
            unmap <buffer> n
            let b:fasttimetravel = 0
        endif
    endif
endfun


fun! FastTimetravel()
    if b:timetravel
        let b:fasttimetravel = 1
        unmap <buffer> i
        nmap <buffer> y :call StepTimezone(1)<cr>
        nmap <buffer> n :call StepTimezone(0)<cr>

        while b:timezone < b:nhistory
            call MoveTimezone(+1)
        endwhile
        call StepTimezone(0)
    endif
endfun


fun! StepTimezone(save)
    if b:timetravel && b:fasttimetravel
        if a:save
            call SaveTimezone()
        endif
        call MoveTimezone(-1)
        echo "Do you want to save this timezone (y/n)?"
    endif
endfun


fun! SaveAllTimezones()
    if b:timetravel
        while b:timezone < b:nhistory
            call MoveTimezone(+1)
        endwhile
        call SaveTimezone()
        while b:timezone > 1
            call MoveTimezone(-1)
            call SaveTimezone()
        endwhile
        while b:timezone < b:nhistory
            call MoveTimezone(+1)
        endwhile
    endif
endfun


fun! SaveTimezone()
    if b:timetravel
        let l:fn = expand("%:p") . '-tz-' . b:timezone
        execute 'write' fnameescape(l:fn)
    endif
endfun


fun! MoveTimezone(dir)
    if b:timetravel
        "echom "Moving from ".b:timezone." by ".a:dir." (n=".b:nhistory.")"
        set modifiable
        if(a:dir == -1)
            if b:timezone > 1
                let b:timezone = b:timezone - 1
                let l:msg = execute("undo")
                echo "Now at ".b:timezone
            else
                echo "Can't go back further than that"
            endif
        elseif(a:dir == +1)
            if b:timezone < b:nhistory
                let b:timezone = b:timezone + 1
                let l:msg = execute("redo")
                echo "Now at ".b:timezone
            else
                echo "Can't go forward further than that"
            endif
        endif
        set nomodifiable
    endif
endfun


command Timemachine call StartTimeMachine()
cnoreabbrev timemachine Timemachine
