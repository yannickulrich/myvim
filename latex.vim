setlocal spelllang=en_gb

fun! CloseEnv()
    let line=getline(line(".")-1)
    if line =~ "\\\\begin{.*}$"
        let env = matchlist(line, '\v\\begin\{([^}]*)\}')[1]
        let endenv = "\n\\end{".env."}\<Up>"
        return endenv
    else
        return ""
    endif
endfun

fun! CompileLaTeX()
    let firstline = getline(1)
    if firstline =~ "%!TEX root"
        let line=getline(2)
        let filename=matchlist(firstline, 'root=\(.*\)')[1]
    else
        let line=firstline
        let filename = expand("%:r")
    endif
    if line =~ "%PDFLATEX"
        let compiler = "pdflatex --shell-escape ".filename.".tex"
    else
        let compiler = "latex ".filename.".tex"
    endif
    execute "!".compiler
endfun

"        let compiler = "latexmk -dvi --ps- --pdf- ".filename.".tex"
fun! OpenOutput()
    let firstline = getline(1)
    if firstline =~ "%!TEX root"
        let line=getline(2)
        let filename=matchlist(firstline, 'root=\(.*\)')[1]
    else
        let line=firstline
        let filename = expand("%:r")
    endif
    if line =~ "%PDFLATEX"
        let command = "evince ".filename.".pdf &"
        execute "!cd `dirname %` && pwd && ".command
    else
        let command = "xdvi ".filename.".dvi &"
        execute "!".command
    endif
endfun

" to be executed by user by C-X C-U
fun! BIBComplete(findstart, base)
  if a:findstart && (b:bib_completion == 'ENABLE' || b:bib_completion == 'REF' )
    let l:line = getline('.')
    let l:start = col('.') - 1
    while l:start > 0 && l:line[l:start - 1] =~ '\a'
      let l:start -= 1
    endwhile
    return start
  elseif b:bib_completion == 'ENABLE'
    " Record what matches − we pass this to complete() later
    let l:res = []
    " Find bib matches
    for l:line in b:biblio
    " Check if it matches what we're trying to complete
      if l:line.key =~ '^' . a:base
      " It matches! See :help complete() for the full docs on the key names
      " for this dict.
        call add(l:res, {
          \ 'icase': 1,
          \ 'word': l:line.key,
          \ 'abbr': l:line.key,
          \ 'menu': l:line.title,
          \ })
      endif
    endfor

    return res
  endif
endfun

fun! FindLabels()
    echo "Matching"

    " Compile current list of lables
    let b:labels = []
    let l:match = 1
    let l:matched = [1]
    while len(l:matched) > 1
        echo "Starting ".l:match
        let l:matched = matchlist(join(getline(1, '$'),'\n'), 'label{\([^}]*\)}',1,l:match)
        echo "Found ".l:matched
        if len(l:matched) > 1
            call add(b:labels,l:matched[1])
            let l:match = l:match+1
        endif
    endwhile
    echo b:labels
endfun

fun! LoadBibTex(filename)
    let l:bibfile=split(join(readfile(a:filename),'||'),'@')
    let b:biblio = []
    for l:entry in l:bibfile[1:]
        try
            let l:lines = split(l:entry,'||')
            let l:type = split(l:lines[0],'{')[0]
            if l:type =~ "comment"
                continue
            endif
            let l:key  = split(split(l:lines[0],'{')[1],',')[0]
            let l:isgood = 0

            let l:bibitem = {'author': '', 'title': '', 'type': l:type, 'key': l:key}
            for l:prop in l:lines[1:]
                if l:prop =~ 'Author' || l:prop =~ 'author'
                    let l:bibitem.author = split(l:prop,'=')[1]
                    let l:isgood = 1
                elseif l:prop =~ 'Title' || l:prop =~ 'title'
                    let l:bibitem.title = split(l:prop,'=')[1]
                    let l:isgood = 1
                endif
            endfor
            if l:isgood == 1
                call add(b:biblio, l:bibitem)
            endif
        catch /.*/
            echo "Problematic line"
            echo l:entry
        endtry
    endfor

endfun

fun! QueryCite()
    let line=strpart(getline("."),0,col("."))
    if line =~ "\\\\cite{$"
        let b:bib_completion = 'ENABLE'
        return "\<C-X>\<C-U>"
    elseif line =~ "\\\\cite{[^}]*$"
        let b:bib_completion = 'ENABLE'
        return "\<C-X>\<C-U>"
    else
        let b:bib_completion = 'DISABLE'
        return ""
    endif
endfun




fun! InitLaTeX()
    setlocal spell spelllang=en_gb
    setlocal tw=0
    setlocal linebreak

    nmap <buffer> <C-T> :call CompileLaTeX()<cr>
    nmap <buffer> <C-P> :call OpenOutput()<cr>
    nmap <buffer> <C-H> :!cd `dirname %` && pwd && rm -f *.auxlock *.aux *bbl *.blg *.log *.nav *.out *.snm *.synctex.gz *.toc *vrb *.synctex.gz\(busy\) *.bcf *.dvi
    imap <buffer> <CR> <CR><C-R>=CloseEnv()<CR>


    set completefunc=BIBComplete
    let b:biblio = []
    " modify completition options, more info in :help completeopt or
    " http://vimdoc.sourceforge.net/htmldoc/options.html#'completeopt'
    set completeopt=longest,menu


    let l:texfile = join(getline(1,line('$')))
    if l:texfile =~ "bibliography"
        let l:bibarg = matchlist(l:texfile,'\v\\bibliography\{([^}]*)\}')[1]
        if l:bibarg =~ "\.bib"
            let l:filename = l:bibarg
        else
            let l:filename = l:bibarg . ".bib"
        endif
    else
        let l:filename = "/home/yannickulrich/Dropbox/PSI/muon_ref.bib"
    endif
    echom "Loading BibTeX file " . l:filename
    call LoadBibTex(l:filename)


    imap <buffer> { {<C-R>=QueryCite()<CR>
    imap <buffer> , ,<C-R>=QueryCite()<CR>



endfun

autocmd BufNewFile,BufRead *.tex call InitLaTeX()
