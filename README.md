# Vim plugins

This is a random collection of vim plugins I have written over time.
Most likely, they are quite specific to whatever I wanted to
accomplish at the time and probably not very useful for other people.

## Installation
```bash
$ git clone git@gitlab.com:yannickulrich/myvim ~/.vim/plugin
```

## Plugins

### IPython
The script `ip.vim` allows you to execute code selected in vim in a
jupyter kernel. For this, run
```bash
$ jupyter console
```
in the command line and connect vim by running `:IPython`. To execute
code, select it and press `<C-T>`. If no code is selected, this will
attempt to execute the current code folding cell. This is meant to be
used together with [py2nb](https://gitlab.com/mule-tools/py2nb).

### Markdown
The script `markdown.vim` uses GitLab's Markdown rendering API for the
current repository on `<C-T>`. To use this, you need an API token that
is accessible through [pass](https://www.passwordstore.org/)
```bash
$ pass gitlab-token
xxxxxxxxxxxxxxxxxxxx
```

### LaTeX
`latex.vim` supports basic LaTeX task such as keeping track of
bibkeys, compilation (`<C-T>`) and auto-closing of environments.
Compilation will use `latex` per default unless `%PDFLATEX` is in the
top two lines of the document. `%!TEX root` is supported.
