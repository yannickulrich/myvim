python3 << EOF
from jupyter_client import KernelManager
import vim
import base64
import os
import re
from jupyter_core.paths import jupyter_runtime_dir
import json


basepath = jupyter_runtime_dir()
kc = None
kname = None

def ConnectKernel(pid=-1):
    if pid == -1:
        pids = [
            re.match("kernel-(\\d*).json", i)
            for i in os.listdir(basepath)
        ]
        pids = [i.group(1) for i in pids if i]
        if len(pids) == 0:
            print("No IPython instances found")
            return
        elif len(pids) > 1:
            print("Multiple IPython instances found: " + ", ".join(pids))
            return
        else:
            pid = pids[0]
    global kc
    global kname

    with open(basepath + "/kernel-%s.json" % pid) as fp:
        kname = json.load(fp)['kernel_name']
        if kname == '':
            kname = 'python3'

    km = KernelManager(
        connection_file=basepath + "/kernel-%s.json" % pid
    )
    km.load_connection_file()
    kc = km.client()
    kc.start_channels()

    if kname == 'python3':
        kc.execute(
            'import os, base64',
            silent=True, store_history=False
        )
    if len(vim.eval("maparg('<C-T>','v')")) > 0:
        vim.command("vunmap <buffer> <C-T>")
    if len(vim.eval("maparg('<C-T>','n')")) > 0:
        vim.command("nunmap <buffer> <C-T>")
    vim.command("vmap <buffer> <C-T> :python3 selector()<cr>")
    vim.command("nmap <buffer> <C-T> :<c-u>normal! [zV]z<cr>:python3 selector()<cr>")
    print("Connected to %s (%s)" % (pid, kname))

def run(cell):
    global kc
    ans = kc.execute_interactive(cell)
    try:
        ec = ans['content']['execution_count']
    except:
        ec = 0
    outp = "In [%d]: " % ec
    outp = "\n"+outp + (
        "\n"+"...: ".rjust(len(outp))
    ).join(cell.split('\n'))+"\n"

    if kname == 'python3':
        cmd = "os.write(1,base64.b64decode(\"%s\"))"
    elif kname == 'wolframlanguage12.3':
        cmd = 'Write[Streams["stderr"],ImportString["%s", "Base64"]];'

    kc.execute(
        cmd % (
            base64.b64encode(outp.encode("ascii")).decode('ascii')
        ),
        silent=True, store_history=False
    )

def selector():
    buf = vim.current.buffer
    (lnum1, col1) = buf.mark('<')
    (lnum2, col2) = buf.mark('>')
    lines = vim.eval('getline({}, {})'.format(lnum1, lnum2))
    lines[0] = lines[0][col1:]
    lines[-1] = lines[-1][:col2]
    run("\n".join(lines))
EOF

command! -nargs=* IPython python3 ConnectKernel(<f-args>)

