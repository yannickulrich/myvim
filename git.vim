fun! GitDiff()
    " Save position
    let l:cursor_pos = getpos(".")
    let l:window = winnr()
    let l:syntax = &syntax

    " Open split to the right
    set splitright

    " Turn on diff in old view
    diffthis

    " Create new view with diff
    let l:filename = expand("%:")
    vnew
    setlocal ma
    execute("-1r! git show HEAD:".l:filename)

    " Make diff unmodifiable
    setlocal noma
    let &modified = 0

    " Copy syntax
    let &syntax=l:syntax
    diffthis

    " Switch back and restore
    exe l:window . "wincmd w"
    call setpos('.', l:cursor_pos)
    %foldopen!
endfun


" nmap <buffer> <C-D> :call GitDiff()<cr>
